package edu.towson.cosc435.ndiong.tipcalculator

import android.media.Image
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.selection.selectable
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import edu.towson.cosc435.ndiong.tipcalculator.ui.theme.TipCalculatorTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            TipCalculatorTheme {
                MainScreen()
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    TipCalculatorTheme {
        MainScreen()
    }
}

@Composable
fun MainScreen() {
    val modifier = Modifier.padding(8.dp)
    val radioOptions = listOf("10%", "20%", "30%")
    var value by savedInstanceState(saver = TextFieldValue.Saver) { TextFieldValue() }
    val (selectedOption, onOptionSelected) = remember { mutableStateOf(radioOptions[0]) }
    val tip = remember { mutableStateOf(0.00) }
    val tipPercentage = when (selectedOption) {
        radioOptions[0] -> 0.1
        radioOptions[1] -> 0.2
        else -> 0.3
    }

    Column(
        modifier = Modifier
            .padding(16.dp)
            .fillMaxSize()
    ) {
        Row(
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(text = "Tip Calculator")
            OutlinedTextField(
                label = { Text("Enter your check amount") },
                value = value,
                onValueChange = {
                    value = it
                    tip.value = calculateTip(value.text, tipPercentage)
                },
                modifier = modifier
            )
        }

        radioOptions.forEach { text ->
            Row(
                modifier.fillMaxWidth()
                    .selectable(
                        selected = (text == selectedOption),
                        onClick = {
                            onOptionSelected(text)
                            tip.value = calculateTip(value.text, tipPercentage)
                        }
                    )
            ) {
                RadioButton(
                    selected = (text == selectedOption),
                    onClick = {
                        onOptionSelected(text)
                        tip.value = calculateTip(value.text, tipPercentage)
                    }
                )
                Text(
                    text = text,
                    modifier = Modifier.padding(start = 8.dp)
                )
            }

        }
        Row(
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically
        )
        {
            Button(onClick = { tip.value = calculateTip(value.text, tipPercentage) }) {
                Text(text = "Calculate")
            }
        }
        Divider(modifier = Modifier.padding(8.dp))
        Text(
            "Your calculated tip is $${tip.value} and your total is $${tip.value + value}",
            modifier = modifier.align(Alignment.End))
    }
}

fun calculateTip(checkAmount: Integer, tipPercentage: Double): Double {
    return (checkAmount * tipPercentage) + checkAmount
}